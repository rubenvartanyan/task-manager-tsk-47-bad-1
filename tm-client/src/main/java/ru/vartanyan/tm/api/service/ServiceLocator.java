package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.IPropertyService;

public interface ServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

}
