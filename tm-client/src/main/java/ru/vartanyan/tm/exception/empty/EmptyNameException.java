package ru.vartanyan.tm.exception.empty;

public class EmptyNameException extends Exception{

    public EmptyNameException() throws Exception {
        super("Error! Name cannot be null or empty...");
    }

}
