package ru.vartanyan.tm.exception.empty;

public class EmptyDescriptionException extends Exception {

    public EmptyDescriptionException() throws Exception {
        super("Error! Description cannot be null or empty...");
    }


}
