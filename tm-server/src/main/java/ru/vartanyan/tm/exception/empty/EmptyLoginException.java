package ru.vartanyan.tm.exception.empty;

public class EmptyLoginException extends Exception {

    public EmptyLoginException()  {
        super("Error! Login cannot be null or empty...");
    }

}
