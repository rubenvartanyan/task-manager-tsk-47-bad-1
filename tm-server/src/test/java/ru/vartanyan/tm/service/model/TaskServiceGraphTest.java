package ru.vartanyan.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.api.service.model.ITaskServiceGraph;
import ru.vartanyan.tm.api.service.model.IUserServiceGraph;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.dto.Task;
import ru.vartanyan.tm.model.TaskGraph;
import ru.vartanyan.tm.model.UserGraph;
import ru.vartanyan.tm.service.ConnectionService;
import ru.vartanyan.tm.service.PropertyService;
import ru.vartanyan.tm.service.TestUtil;

import java.util.ArrayList;
import java.util.List;

public class TaskServiceGraphTest {

    @NotNull
    private final ServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final IConnectionService connectionService = serviceLocator.getConnectionService();

    @NotNull
    private final ITaskServiceGraph taskService = serviceLocator.getTaskService();

    @NotNull
    private final IUserServiceGraph userService = serviceLocator.getUserService();

    {
        TestUtil.initUser();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<TaskGraph> tasks = new ArrayList<>();
        final TaskGraph task1 = new TaskGraph();
        final TaskGraph  task2 = new TaskGraph();
        tasks.add(task1);
        tasks.add(task2);
        taskService.addAll(tasks);
        Assert.assertTrue(taskService.findOneById(task1.getId()) != null);
        Assert.assertTrue(taskService.findOneById(task2.getId()) != null);
        taskService.remove(tasks.get(0));
        taskService.remove(tasks.get(1));
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() throws NullObjectException {
        final TaskGraph  task = new TaskGraph();
        taskService.add(task);
        Assert.assertNotNull(taskService.findOneById(task.getId()));
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void clearTest() {
        taskService.clear();
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        final int taskSize = taskService.findAll().size();
        final List<TaskGraph> tasks = new ArrayList<>();
        final TaskGraph  task1 = new TaskGraph();
        final TaskGraph  task2 = new TaskGraph();
        tasks.add(task1);
        tasks.add(task2);
        taskService.addAll(tasks);
        Assert.assertEquals(2 + taskSize, taskService.findAll().size());
        taskService.remove(task1);
        taskService.remove(task2);
    }
    
    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() throws NullObjectException {
        final TaskGraph  task = new TaskGraph();
        final String taskId = task.getId();
        taskService.add(task);
        Assert.assertNotNull(taskService.findOneById(taskId));
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() throws NullObjectException {
        final TaskGraph task = new TaskGraph();
        UserGraph user = userService.findByLogin("test");
        String userId = user.getId();
        task.setUser(userService.findByLogin("test"));
        taskService.add(task);
        Assert.assertTrue(taskService.findOneByIndex(userId, 1) != null);
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTestByUserId() throws NullObjectException {
        final TaskGraph  task = new TaskGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        task.setUser(userGraph);
        taskService.add(task);
        final String taskId = task.getId();
        Assert.assertTrue(taskService.findOneById(userId, taskId) != null);
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByNameTest() throws NullObjectException {
        final TaskGraph  task = new TaskGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        task.setUser(userGraph);
        task.setName("pr1");
        taskService.add(task);
        final String name = task.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(taskService.findOneByName(userId, name) != null);
        taskService.remove(task);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() throws NullObjectException {
        final TaskGraph  task = new TaskGraph();
        taskService.add(task);
        final String taskId = task.getId();
        taskService.removeOneById(taskId);
        Assert.assertTrue(taskService.findOneById(taskId) == null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTestByUserId() throws NullObjectException {
        final TaskGraph  task = new TaskGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        task.setUser(userGraph);
        taskService.add(task);
        final String taskId = task.getId();
        taskService.removeOneById(userId, taskId);
        Assert.assertTrue(taskService.findOneById(taskId) == null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIndexTest() throws NullObjectException {
        final TaskGraph  task1 = new TaskGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        task1.setUser(userGraph);
        taskService.add(task1);
        taskService.removeOneByIndex(userId, 1);
        Assert.assertTrue(taskService.findOneByIndex(userId, 1) == null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByNameTest() throws NullObjectException {
        final TaskGraph  task = new TaskGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        task.setUser(userGraph);
        task.setName("pr1");
        taskService.add(task);
        final String name = task.getName();
        Assert.assertNotNull(name);
        taskService.removeOneByName(userId, name);
        Assert.assertTrue(taskService.findOneByName(userId, name) == null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() throws NullObjectException {
        final TaskGraph  task = new TaskGraph();
        taskService.add(task);
        taskService.remove(task);
        Assert.assertNull(taskService.findOneById(task.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void removeTestByUserIdAndObject() throws NullObjectException {
        final TaskGraph  task = new TaskGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        task.setUser(userGraph);
        taskService.add(task);
        taskService.remove(userId, task);
        Assert.assertFalse(taskService.findOneById(task.getId()) != null);
    }

}
