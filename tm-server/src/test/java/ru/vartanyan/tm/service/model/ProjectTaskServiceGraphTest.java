package ru.vartanyan.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.api.service.model.IProjectServiceGraph;
import ru.vartanyan.tm.api.service.model.IProjectTaskServiceGraph;
import ru.vartanyan.tm.api.service.model.ITaskServiceGraph;
import ru.vartanyan.tm.api.service.model.IUserServiceGraph;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.model.ProjectGraph;
import ru.vartanyan.tm.model.TaskGraph;
import ru.vartanyan.tm.model.UserGraph;
import ru.vartanyan.tm.service.TestUtil;

public class ProjectTaskServiceGraphTest {

    @NotNull
    private final ServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final IConnectionService connectionService = serviceLocator.getConnectionService();

    @NotNull
    private final IProjectTaskServiceGraph projectTaskService = serviceLocator.getProjectTaskService();

    @NotNull
    private final ITaskServiceGraph taskService = serviceLocator.getTaskService();

    @NotNull
    private final IProjectServiceGraph projectService = serviceLocator.getProjectService();

    @NotNull
    private final IUserServiceGraph userService = serviceLocator.getUserService();

    {
        TestUtil.initUser();
    }

    @Test
    @Category(DBCategory.class)
    public void bindTaskByProjectIdTest() throws NullObjectException {
        final TaskGraph task = new TaskGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        final ProjectGraph project = projectService.add(userId, "testBind", "-");
        final String projectId = project.getId();
        final String taskId = task.getId();
        task.setUser(userGraph);
        taskService.add(task);
        projectTaskService.bindTaskByProject(userId, projectId, taskId);
        Assert.assertTrue(taskService.findOneById(userId, taskId) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByProjectIdTest() throws NullObjectException {
        final TaskGraph task = new TaskGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        final ProjectGraph project = projectService.add(userId, "testFindAll", "-");
        final String projectId = project.getId();
        task.setUser(userGraph);
        task.setProject(project);
        taskService.add(task);
        Assert.assertFalse(projectTaskService.findAllByProjectId(userId, projectId).isEmpty());
        Assert.assertEquals(1, projectTaskService.findAllByProjectId(userId, projectId).size());

        final TaskGraph task2 = new TaskGraph();
        task2.setUser(userGraph);
        task2.setProject(project);
        taskService.add(task2);
        Assert.assertEquals(2, projectTaskService.findAllByProjectId(userId, projectId).size());

        final TaskGraph task3 = new TaskGraph();
        final @NotNull UserGraph userGraph2 = userService.findByLogin("test2");
        final String user2Id = userGraph2.getId();
        task3.setUser(userGraph2);
        task3.setProject(project);
        taskService.add(task3);
        Assert.assertEquals(2, projectTaskService.findAllByProjectId(userId, projectId).size());
        Assert.assertEquals(1, projectTaskService.findAllByProjectId(user2Id, projectId).size());

        final TaskGraph task4 = new TaskGraph();
        final ProjectGraph project2 = projectService.add(userId, "testFindAll2", "-");
        final String project2Id = project2.getId();
        task4.setUser(userGraph);
        task4.setProject(project2);
        taskService.add(task4);
        Assert.assertEquals(2, projectTaskService.findAllByProjectId(userId, projectId).size());
        Assert.assertEquals(1, projectTaskService.findAllByProjectId(userId, project2Id).size());
    }

    @Test
    @Category(DBCategory.class)
    public void removeAllByProjectIdTest() throws NullObjectException {
        final TaskGraph task = new TaskGraph();
        final TaskGraph task2 = new TaskGraph();
        final TaskGraph task3 = new TaskGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        final ProjectGraph project = projectService.add(userId, "testBind", "-");
        final String projectId = project.getId();
        task.setUser(userGraph);
        task.setProject(project);
        task2.setUser(userGraph);
        task2.setProject(project);
        task3.setUser(userGraph);
        task3.setProject(project);
        taskService.add(task);
        taskService.add(task2);
        taskService.add(task3);
        Assert.assertEquals(3, projectTaskService.findAllByProjectId(userId, projectId).size());
        projectTaskService.removeProjectById(userId, projectId);
        Assert.assertTrue(projectTaskService.findAllByProjectId(userId, projectId).isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void unbindTaskFromProjectIdTest() throws NullObjectException {
        final TaskGraph task = new TaskGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        final String taskId = task.getId();
        task.setUser(userGraph);
        taskService.add(task);
        projectTaskService.unbindTaskFromProject(userId, taskId);
        Assert.assertTrue(taskService.findOneById(userId, taskId) != null);
        projectTaskService.unbindTaskFromProject(userId, taskId);
        final TaskGraph task2 = taskService.findOneById(userId, taskId);
        Assert.assertNull(task2.getProject());
    }

}
